package competizionecanora;

public class Cantante {
    private String nome;
    private String canzone;
    
    public Cantante(String nome, String canzone) {
        this.nome = nome;
        this.canzone = canzone;
    }
    
    public String getNome() {
        return this.nome;
    }
    public String getCanzone() {
        return this.canzone;
    }
    
    @Override
    public String toString() {
        return this.nome + " - " + this.canzone;
    }
    
    @Override
    public boolean equals(Object altro) {
        if(this == altro) {
            return true;
        }
        
        if(altro == null) {
            return false;
        }
        
        if(this.getClass() != altro.getClass()) {
            return false;
        }
        
        Cantante p = (Cantante) altro;
        
        return this.nome == p.nome && this.canzone == p.canzone;
    }

}
