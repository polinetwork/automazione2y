package competizionecanora;

public class CompetizioneCanora {

    private String presentatore;
    private String luogo;
    private String data;
    private Cantante[] cantanti;
    private int indiceCantanti;
    
    public CompetizioneCanora(String presentatore, String luogo, String data, int numeroCantanti) {
        this.presentatore = presentatore;
        this.luogo = luogo;
        this.data = data;
        this.cantanti = new Cantante[numeroCantanti];
        this.indiceCantanti = 0;
    }
    
    public void aggiungiCantante(Cantante cantante) {
        if(this.indiceCantanti == this.cantanti.length) {
            System.out.println("Numero massimo cantanti raggiunto!");
            return;
        }
        this.cantanti[this.indiceCantanti] = cantante;
        this.indiceCantanti++;  
    }
    
    public void aggiungiCantante(Cantante cantante, int posizione) {
        posizione--; // da "umano" (1-10) a "computer" (0-9)
        if(posizione < 0 || posizione >= this.cantanti.length) {
            System.out.println("Posizione "+posizione+" non valida!");
            return;
        }
        if(this.indiceCantanti == this.cantanti.length) {
            System.out.println("Numero massimo cantanti raggiunto!");
            return;
        }
        
        // scorro al contrario
        // devo spostare tutti i cantanti a destra di una posizione
        for(int i = this.indiceCantanti; i > posizione; i--) {
            this.cantanti[i] = this.cantanti[i-1];
        }
        this.cantanti[posizione] = cantante;
        this.indiceCantanti++;
    }
    
    public int trovaCantante(Cantante cantante) {
        for(int i = 0; i < this.indiceCantanti; i++) {
            if(this.cantanti[i].equals(cantante)) {
                return i;
            }
        }
        System.out.println("Cantante non trovato!");
        return -1;
    }
    
    public void rimuoviCantante(Cantante cantante) {
        int indice = this.trovaCantante(cantante);
        if (indice == -1) {
            return;
        }
        for(int i = indice; i < this.indiceCantanti-1; i++) {
            this.cantanti[i] = this.cantanti[i+1];
        }
        this.indiceCantanti--;
    }
    
    public String toString() {
        String str = "Competizione Canora\n";
        str += "Presentatore: " + this.presentatore + "\n";
        str += "Luogo: " + this.luogo + "\n";
        str += "Data: " + this.data + "\n\n";
        
        for(int i = 0; i < this.indiceCantanti; i++) {
            str += (i+1) + ": " + this.cantanti[i] + "\n";
        }
        
        return str;
    }
    
    public static void main(String[] args) {
        CompetizioneCanora competizione = new CompetizioneCanora("Andrea", "Polimi", "13/11/2020", 5);
        competizione.aggiungiCantante(new Cantante("Fedez", "Psichedelico"));
        competizione.aggiungiCantante(new Cantante("David Guetta", "Titanium"));
        competizione.aggiungiCantante(new Cantante("Breathe Carolina", "Rhythm Is A Dancer"));
        competizione.aggiungiCantante(new Cantante("blink-182", "All The Small Things"), 1);
        
        Cantante sum41 = new Cantante("Sum41", "Pieces");
        competizione.aggiungiCantante(sum41, 2);
        
        System.out.println(competizione);
        
        competizione.rimuoviCantante(sum41);
        
        System.out.println(competizione);
        
        Cantante sum41new = new Cantante("Sum41", "With Me");
        competizione.aggiungiCantante(sum41new, 1);
        System.out.println(competizione);
        
        
    }

}
