package set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MySet {
    
    private int elementi[];
    private int dimensione_corrente;
    
    public MySet(int dimensione) {
        this.elementi = new int[dimensione];
        this.dimensione_corrente = 0;
    }
    
    public MySet() {
        this(100);
    }
    
    @Override
    public String toString() {
        String str = new String("Set: {");
        
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(i > 0) {
                str += ", ";
            }
            
            str += this.elementi[i];
        }
        
        str += "}";
        
        return str;
    }
    
    public boolean elementoPresente(int elemento) {
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(this.elementi[i] == elemento) {
                return true;
            }
        }
        
        return false;
    }
    
    public void aggiungiElemento(int elemento) {
        if(this.elementoPresente(elemento)) {
            return;
        }
        
        if(this.dimensione_corrente == this.elementi.length) {
            return;
        }
        
        this.elementi[this.dimensione_corrente] = elemento;
        this.dimensione_corrente++;
    }
    
    public int trovaElemento(int elemento) {
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(this.elementi[i] == elemento) {
                return i;
            }
        }
        
        return -1;        
    }
    
    public void rimuoviElemento(int elemento) {
        int posizione = this.trovaElemento(elemento);
        
        if(posizione == -1) {
            return;
        }
        
        this.dimensione_corrente--;
        
        while(posizione < this.dimensione_corrente) {
            this.elementi[posizione] = this.elementi[posizione+1];
            posizione++;
        }
    }
    
    public int sommaFor() {
        int somma = 0;
        
        for(int i = 0; i < this.dimensione_corrente; i++) {
            somma = somma + this.elementi[i];
        }
        
        return somma;
    }
    
    public int sommaWhile() {
        int somma = 0;
        
        int i = 0;
        
        while(i < this.dimensione_corrente) {
            somma = somma + this.elementi[i];
            i++;
        }
        
        return somma;
    }
    
    public int sommaForEach() {
        int somma = 0;
        
        for(int elemento: this.elementi) {
            somma = somma + elemento;
        }
        
        return somma;
    }
    
    public int sommaIteratore() {
        int somma = 0;
        
        Set<Integer> set_java = new HashSet<Integer>();
        
        for(int i = 0; i < this.dimensione_corrente; i++) {
            set_java.add(this.elementi[i]);
        }
        
        Iterator<Integer> iter = set_java.iterator();
        
        while(iter.hasNext()) {
            somma += iter.next();
        }
        
        return somma;
    } 
    
    public static void main(String[] args) {
        MySet s = new MySet(10);
        s.aggiungiElemento(10);
        s.aggiungiElemento(5);
        s.aggiungiElemento(12);
        s.aggiungiElemento(3);
        
        System.out.println(s);
        s.rimuoviElemento(5);
        System.out.println(s);
        
        System.out.println(s.sommaFor());
        System.out.println(s.sommaWhile());
        System.out.println(s.sommaForEach());
        
        System.out.println(s.sommaIteratore());
    }
    
}
