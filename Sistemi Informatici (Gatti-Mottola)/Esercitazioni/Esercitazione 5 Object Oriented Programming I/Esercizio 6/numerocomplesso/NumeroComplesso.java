package numerocomplesso;

public class NumeroComplesso {
    private double re, img;
    
    public NumeroComplesso(double re, double img) {
        this.re = re;
        this.img = img;
    }
    
    public double getParteReale() {
        return this.re;
    }
    
    public double getParteImmaginaria() {
        return this.img;
    }
    
    public double modulo() {
        return Math.sqrt(this.re * this.re + this.img * this.img);
    }
    
    public double fase() {
        if(this.re == 0) {
            // Fase PiGreco Mezzi
            return Math.PI/2;
        } else {
            return Math.atan(img/re);
        }
    }
    
    public NumeroComplesso somma(NumeroComplesso altro) {
        return new NumeroComplesso(this.re + altro.re, this.img + altro.img);
    }

    public NumeroComplesso differenza(NumeroComplesso altro) {
        return new NumeroComplesso(this.re - altro.re, this.img - altro.img);
    }
    
    @Override
    public String toString() {
        String str = "";
        
        if(this.re != 0) {
            str += this.re;
        }
        
        if(this.img != 0) {
            if(str.length() > 0) {
                str += " + ";
            }
            
            str += "i" + this.img;
        }
        
        return str;
    }

    public static void main(String[] args) {
        NumeroComplesso x = new NumeroComplesso(0, 1);
        System.out.println(x);
        System.out.println(x.modulo());
        System.out.println(x.fase());
        
        NumeroComplesso y = new NumeroComplesso(1, 0);
        System.out.println(y);
        
        NumeroComplesso z;
        
        z = x.differenza(y);
        System.out.println(z);
        
        z = x.somma(y);
        System.out.println(z);
    }
    
}
