package set;

public class Set {
    
    private int elementi[];
    private int dimensione_corrente;
    
    public Set(int dimensione) {
        this.elementi = new int[dimensione];
        this.dimensione_corrente = 0;
    }
    
    public Set() {
        this(100);
    }
    
    @Override
    public String toString() {
        String str = new String("Set: {");
        
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(i > 0) {
                str += ", ";
            }
            
            str += this.elementi[i];
        }
        
        str += "}";
        
        return str;
    }
    
    public boolean elementoPresente(int elemento) {
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(this.elementi[i] == elemento) {
                return true;
            }
        }
        
        return false;
    }
    
    public void aggiungiElemento(int elemento) {
        if(this.elementoPresente(elemento)) {
            return;
        }
        
        if(this.dimensione_corrente == this.elementi.length) {
            return;
        }
        
        this.elementi[this.dimensione_corrente] = elemento;
        this.dimensione_corrente++;
    }
    
    public int trovaElemento(int elemento) {
        for(int i = 0; i < this.dimensione_corrente; i++) {
            if(this.elementi[i] == elemento) {
                return i;
            }
        }
        
        return -1;        
    }
    
    public void rimuoviElemento(int elemento) {
        int posizione = this.trovaElemento(elemento);
        
        if(posizione == -1) {
            return;
        }
        
        this.dimensione_corrente--;
        
        while(posizione < this.dimensione_corrente) {
            this.elementi[posizione] = this.elementi[posizione+1];
            posizione++;
        }
    }

    public static void main(String[] args) {
        Set s = new Set(10);
        s.aggiungiElemento(10);
        s.aggiungiElemento(5);
        s.aggiungiElemento(12);
        s.aggiungiElemento(3);
        
        System.out.println(s);
        s.rimuoviElemento(5);
        System.out.println(s);
    }
    
}
