package stack;

public class Stack {
    
    private int dati[];
    private int ultimo;
    
    public Stack(int dimensione) {
        this.dati = new int[dimensione];
        // prima cella vuota
        this.ultimo = 0;
    }
    
    public void push(int dato) {
        // stack pieno
        if(this.dati.length == ultimo) {
            System.out.println("Stack pieno: impossibile aggiungere un elemento allo stack!");
            return;
        }
        
        this.dati[this.ultimo] = dato;
        this.ultimo += 1;
    }
    
    public int pop() {
        // stack vuoto
        if(this.ultimo == 0) {
            System.out.println("Stack vuoto: impossibile leggere un elemento dallo stack!");
            return -1;
        }
        this.ultimo--;
        return this.dati[this.ultimo];
    }
    
    public String toString() {
        String str = "Stack(";
        for(int i = 0; i < this.ultimo; i++) {
            if(i > 0) {
                str += ", ";
            }
            
            str += this.dati[i];
        }
        
        str += ")";
        
        return str;
    }
    
    public static void main(String[] args) {
        Stack s = new Stack(3);

        s.push(0);
        System.out.println(s);
        s.push(1);
        System.out.println(s);
        s.push(2);
        System.out.println(s);
        System.out.println("POP: "+s.pop());
        System.out.println(s);
        System.out.println("POP: "+s.pop());
        System.out.println(s);
        System.out.println("POP: "+s.pop());
        System.out.println(s);
        // Vuoto
        System.out.println("POP: "+s.pop());

        s.push(0);
        System.out.println(s);
        s.push(1);
        System.out.println(s);
        s.push(2);
        System.out.println(s);
        // Stack pieno
        s.push(3);
    }
    
}
