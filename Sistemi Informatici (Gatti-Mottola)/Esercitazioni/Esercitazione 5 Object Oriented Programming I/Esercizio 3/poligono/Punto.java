package poligono;

public class Punto {
    private float x;
    private float y;
    
    public Punto(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString() {
        return "Punto(" + this.x + ", " + this.y + ")";
    }
    
    public float distanza(Punto p) {
        float x_diff = this.x - p.x;
        float y_diff = this.y - p.y;
        
        return (float) Math.sqrt(x_diff * x_diff + y_diff * y_diff);
    }
}
