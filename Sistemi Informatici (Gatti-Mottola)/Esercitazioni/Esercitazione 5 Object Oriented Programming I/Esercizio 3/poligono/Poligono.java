package poligono;

public class Poligono {
    
    private Punto[] vertici;
    
    public Poligono(int n_vertici) {
        this.vertici = new Punto[n_vertici];
    }
    
    public void impostaVertice(int n, float x, float y) {
        if(n < 1 || n > this.vertici.length) {
            System.err.println("Errore: vertice fuori dai limiti!");
            return;
        }
        
        this.vertici[n-1] = new Punto(x, y);
    }
    
    public float perimetro() {
        Punto punto;
        Punto punto_succ;
        
        float p;
        
        punto = this.vertici[this.vertici.length - 1];
        punto_succ = this.vertici[0];
        
        p = punto.distanza(punto_succ);
        
        for(int i = 0; i < this.vertici.length - 1; i++) {
            punto = this.vertici[i];
            punto_succ = this.vertici[i+1];
            
            p += punto.distanza(punto_succ);
        }
        
        return p;
    }
    
    @Override
    public String toString() {
        String str = "";
        
        for(Punto vertice: this.vertici) {
            str += vertice + " ";
        }
        
        return str;
    }

    public static void main(String[] args) {
        Poligono p = new Poligono(4);
        
        p.impostaVertice(1, 0, 0);
        p.impostaVertice(2, 2, 0);
        p.impostaVertice(3, 2, 2);
        p.impostaVertice(4, 0, 2);
        
        System.out.println(p);
        
        System.out.println("Perimetro: "+p.perimetro());
        
        // TODO code application logic here
    }
    
}
