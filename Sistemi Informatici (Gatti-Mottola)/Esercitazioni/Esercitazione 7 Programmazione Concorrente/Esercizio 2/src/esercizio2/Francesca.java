package esercizio2;

public class Francesca extends Thread {
    private Conto conto;
    
    public Francesca(Conto conto) {
        this.conto = conto;
    }
    
    @Override
    public void run() {
        while(true) {
            int vendita = (int)(Math.random() * 1000);
            this.conto.deposita(vendita);
            
            System.out.println("Francesca ha depositato "+vendita);
            
            try {
                sleep(3000);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
    
}
