package esercizio2;

public class Conto {
    private int saldo;
    
    public Conto(int saldo) {
        this.saldo = saldo;
    }
    
    public synchronized void deposita(int soldi) {
        this.saldo += soldi;
        System.out.println("Nuovo bilancio: "+ this.saldo);
    }
    
    public synchronized boolean preleva(int soldi) {
        if(this.saldo < soldi) {
            return false;
        } else {
            this.saldo -= soldi;
            System.out.println("Nuovo bilancio: "+ this.saldo);
            return true;
        }
    }
    
    public int getSaldo() {
        return this.saldo;
    }
}
