package esercizio2;


public class Esercizio2 {
    
    public static void main(String[] args) {
        Conto conto = new Conto(0);
        Francesca francesca = new Francesca(conto);
        Familiare giacomo = new Familiare(conto, "Giacomo");
        Familiare maurizio = new Familiare(conto, "Maurizio");
        
        francesca.start();
        giacomo.start();
        maurizio.start();
    }
    
}
