package esercizio2;


public class Esercizio2 {
    
    public static void main(String[] args) {
        Conto conto = new Conto(0);
        Francesca francesca = new Francesca(conto);
        Fratello giacomo = new Fratello(conto, "Giacomo");
        Fratello maurizio = new Fratello(conto, "Maurizio");
        
        francesca.start();
        giacomo.start();
        maurizio.start();
    }
    
}
