package esercizio2;

public class Fratello extends Thread {
    private Conto conto;
    private String nome;
    
    public Fratello(Conto conto, String nome) {
        this.conto = conto;
        this.nome = nome;
    }
    
    @Override
    public void run() {
        while(true) {
            int soldi = this.conto.getSaldo();
            
            if(soldi > 0) {
                if(this.conto.preleva(soldi)) {
                    System.out.println(this.nome + " ha prelevato " + soldi);
                } else {
                    System.err.println(this.nome + " non ha prelevato "+soldi);
                }
            }
            
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
