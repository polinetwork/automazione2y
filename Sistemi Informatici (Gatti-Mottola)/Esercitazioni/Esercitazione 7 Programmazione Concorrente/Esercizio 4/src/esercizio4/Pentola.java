package esercizio4;

public class Pentola {
    private final int P = 5;
    private int porzioni;
    
    public Pentola() {
        this.porzioni = this.P;
    }
    
    public synchronized void prendiPorzione() {
        while(this.porzioni == 0) {
            try {
                wait();
            } catch(InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
        
        this.porzioni--;
        
        System.out.println("Porzione presa. Rimanenti: " + this.porzioni);
        notifyAll();
    }
    
    public synchronized void riempiPentola() {
        while(this.porzioni > 0) {
            try {
                wait();
            } catch(InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
        this.porzioni = this.P;
        System.out.println("Pentola riempita!");
        notifyAll();
    }
}
