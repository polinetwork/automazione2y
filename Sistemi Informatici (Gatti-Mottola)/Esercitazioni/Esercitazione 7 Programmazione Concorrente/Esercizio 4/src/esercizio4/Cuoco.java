package esercizio4;

public class Cuoco extends Thread {
    private Pentola pentola;
    
    public Cuoco(Pentola pentola) {
        this.pentola = pentola;
    }
    
    @Override
    public void run() {
        while(true) {
            pentola.riempiPentola();
            System.out.println("Cuoco: pentola riempita!");
        }
    }
}
