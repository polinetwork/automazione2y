package esercizio4;

public class Esercizio4 {

    public static void main(String[] args) {
        Pentola pentola = new Pentola();
        
        Cuoco cuoco = new Cuoco(pentola);
        cuoco.start();
        
        for(int i = 0; i < 10; i++) {
            Campeggiatore campeggiatore = new Campeggiatore(pentola, "C" + i);
            campeggiatore.start();
        }
    }
    
}
