package esercizio4;

public class Campeggiatore extends Thread {
    private Pentola pentola;
    private String name;
    
    public Campeggiatore(Pentola pentola, String name) {
        this.pentola = pentola;
        this.name = name;
    }
    
    @Override
    public void run() {
        while(true) {
            pentola.prendiPorzione();
            System.out.println("Il campeggiatore "+ this.name +" ha preso una porzione."); 
        }
    }
    
}
