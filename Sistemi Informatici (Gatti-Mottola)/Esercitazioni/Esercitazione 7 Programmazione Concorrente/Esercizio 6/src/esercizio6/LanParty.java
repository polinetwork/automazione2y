package esercizio6;

public class LanParty {
    private Giocatore[] giocatori;
    private int computers;
    private int computer_giocatore[];
    private int computer_liberi;
    
    public LanParty(int n_giocatori, int n_computers) {
        // ngiocatori > ncomputers
        
        this.giocatori = new Giocatore[n_giocatori];
        this.computer_giocatore = new int[n_computers];
        
        for(int i = 0; i < n_giocatori; i++) {
            this.giocatori[i] = new Giocatore(i, this);
        }
        
        for(int i = 0; i < n_computers; i++) {
            this.computer_giocatore[i] = -1;
        }
        
        this.computers = n_computers;
        this.computer_liberi = n_computers;
    }
    
    public StatoGiocatore getStatoGiocatore(int id) {
        return this.giocatori[id].getStato();
    }
    
    public synchronized boolean occupaComputer(int id_giocatore, int id_computer) {
        if(this.computer_giocatore[id_computer] != -1) {
            return false;
        }
        this.computer_giocatore[id_computer] = id_giocatore;
        this.computer_liberi--;
        return true;
    }
    
    public synchronized void liberaComputer(int id_computer) {
        this.computer_giocatore[id_computer] = -1;
        this.computer_liberi++;
        this.notifyAll();
    }
        
    public void sedutaIniziale() {
        for(int i = 0; i < this.computers; i++) {
            this.giocatori[i].siediti(i);
        }
    }
    
    public boolean computerLibero() {
        return this.computer_liberi > 0;
    }
    
    public int getComputerLibero() {
        for(int i = 0; i < this.computers; i++) {
            if(this.computer_giocatore[i] == -1) {
                return i;
            }
        }
        return -1;
    }
    
    public int individuaComputer(int id_giocatore) {
        int computer;
        int giocatore;
        
        do {
            computer = (int)(Math.random() * (this.computers));
            giocatore = this.computer_giocatore[computer];
        } while (giocatore == id_giocatore || giocatore == -1);
        
        return giocatore;
    }
    
    public void inizia() {
        for(int i = 0; i < this.giocatori.length; i++) {
            this.giocatori[i].start();
        }
    }
}
