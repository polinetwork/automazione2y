package esercizio6;

public class Giocatore extends Thread {
    private int id;
    private StatoGiocatore stato;
    private LanParty stanza;
    private int computer_occupato;
    
    public Giocatore(int id, LanParty stanza) {
        this.id = id;
        this.stanza = stanza;
        this.stato = StatoGiocatore.SENZA_PC;
        this.computer_occupato = -1;
    }
    
    public StatoGiocatore getStato() {
        return this.stato;
    }
    
    public void siediti(int id_computer) {
        if(this.stanza.occupaComputer(this.id, id_computer)) {
            this.stato = StatoGiocatore.CON_PC;
            this.computer_occupato = id_computer;
            System.out.println("Giocatore" + this.id +" si siede sul computer"+id_computer);
        }
    }
    
    public void alzati() {
        int id_computer = this.computer_occupato;
        this.computer_occupato = -1;
        this.stato = StatoGiocatore.SENZA_PC;
        this.stanza.liberaComputer(id_computer);
        System.out.println("Giocatore" + this.id +" si alza.");
    }
    
    public void rageQuit() {
        
        this.stato = StatoGiocatore.RAGE_QUITTING;
        int id_giocatore = this.stanza.individuaComputer(this.id);
        System.out.println("Giocatore" + this.id +" entra in stato di rage quit e aspetta Giocatore"+id_giocatore);
        
        synchronized(this.stanza) {
            this.stanza.notifyAll();
        }

        while(this.stanza.getStatoGiocatore(id_giocatore) != StatoGiocatore.RAGE_QUITTING) {
            try {
                synchronized(this.stanza) {
                    this.stanza.wait();
                }
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }

        this.alzati();
    }
    
    public void cercaComputer() {
        while(!this.stanza.computerLibero()) {
            try {
                synchronized(this.stanza) {
                    this.stanza.wait();
                }
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
        
        int computer_libero = this.stanza.getComputerLibero();
        
        if(computer_libero > -1) {
            this.siediti(computer_libero);
        }
    }
    
    public void aspetta() {
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    @Override
    public void run() {
        System.out.println("Avvio Giocatore"+this.id);
        while(true) {
            while(this.stato != StatoGiocatore.CON_PC) {
                this.cercaComputer();
            }
            
            if(Math.random() > 0.5) {
                this.rageQuit();
            }
            
            this.aspetta();
        }
    }
}
