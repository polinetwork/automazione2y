package esercizio5;

public class Sezione {
    private boolean occupata;
    private Autore occupante;
    private int id;
    
    public Sezione(int id) {
        this.id = id;
        this.occupata = false;
    }
    
    public synchronized boolean bloccaSezione(Autore autore) {
        if(this.occupata) {
            return false;
        } else {
            this.occupante = autore;
            this.occupata = true;
            return true;
        }
    }
    
    public synchronized boolean sbloccaSezione(Autore autore) {
        if(this.occupante != autore) {
            return false;
        }
        this.occupata = false;
        this.occupante = null;
        return true;
    }
}
