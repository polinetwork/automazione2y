package esercizio5;

public class Esercizio5 {

    public static void main(String[] args) {
        Documento doc = new Documento(6);
        Autore autore1 = new Autore("Andrea", doc);
        Autore autore2 = new Autore("Luca", doc);
        autore1.start();
        autore2.start();
    }
}
