package esercizio5;

import java.util.ArrayList;

public class Autore extends Thread {
    private String nome;
    private Documento documento;
    private ArrayList<Integer> sezioni_bloccate;
    
    public Autore(String nome, Documento documento) {
        this.nome = nome;
        this.documento = documento;
        this.sezioni_bloccate = new ArrayList<Integer>();
    }
    
    public boolean bloccaSezione(int id) {
        if(this.sezioni_bloccate.size() == 2) {
            this.sbloccaSezioneVecchia();
        }
        
        boolean bloccata = this.documento.bloccaSezione(id, this);
        
        if(bloccata) {
            this.sezioni_bloccate.add(id);
            System.out.println(this.nome+" blocca sezione "+id);
            return true;
        } else {
            System.out.println(this.nome+" non blocca sezione "+id);
            return false;
        }        
    }
    
    public void sbloccaSezioneVecchia() {
        int id = this.sezioni_bloccate.get(0);
        System.out.println(this.nome+" sblocca sezione "+id);
        this.documento.sbloccaSezione(id, this);
        this.sezioni_bloccate.remove(0);
    }
    
    @Override
    public void run() {
        while(true) {
            int id = (int)(Math.random() * this.documento.getNumeroSezioni());
            this.bloccaSezione(id);
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
    
}
