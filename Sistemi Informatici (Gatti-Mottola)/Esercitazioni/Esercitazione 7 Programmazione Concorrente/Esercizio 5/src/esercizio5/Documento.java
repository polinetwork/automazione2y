package esercizio5;

public class Documento {
    private Sezione[] sezioni;
    
    public Documento(int numero_sezioni) {
        this.sezioni = new Sezione[numero_sezioni];
        
        for(int i = 0; i < numero_sezioni; i++) {
            this.sezioni[i] = new Sezione(i);
        }
    }
    
    public int getNumeroSezioni() {
        return this.sezioni.length;
    }
    
    public boolean bloccaSezione(int id, Autore autore) {
        return this.sezioni[id].bloccaSezione(autore);
    }
    
    public boolean sbloccaSezione(int id, Autore autore) {
        return this.sezioni[id].sbloccaSezione(autore);
    }
}
