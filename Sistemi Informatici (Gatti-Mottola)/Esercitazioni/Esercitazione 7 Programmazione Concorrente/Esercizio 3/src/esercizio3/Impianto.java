package esercizio3;

public class Impianto extends Thread {
    private Stato stato;
    private Valvola valvola;
    
    public Impianto() {
        this.stato = Stato.NORMALE;
    }
    
    @Override
    public void run() {
        while(this.stato == Stato.NORMALE) {
            while(Math.random() < 0.8) {
                System.out.println("Tutto regolare!");
            }
            System.err.println("Malfunzionamento! Aziono la valvola...");
            this.stato = Stato.MALFUNZIONAMENTO;
            
            this.valvola = new Valvola();
            this.valvola.start();
            
            synchronized(valvola) {
                try {
                    valvola.wait(5000);
                } catch (InterruptedException ex) {
                   System.out.println(ex.getMessage());
                }
            }
            
            if(valvola.aperta()) {
                System.out.println("Valvola azionata!");
                try {
                    valvola.join();
                    this.stato = Stato.NORMALE;
                } catch(InterruptedException ex) {
                   System.out.println(ex.getMessage());
                }
            } else {
                System.err.println("La valvola non si è aperta in tempo!");
                this.stato = Stato.FERMO;
            }
        }
    }
}
