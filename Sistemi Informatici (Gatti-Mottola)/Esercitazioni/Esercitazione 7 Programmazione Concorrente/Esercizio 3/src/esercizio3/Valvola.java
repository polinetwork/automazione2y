package esercizio3;

public class Valvola extends Thread {
    private boolean aperta;
    
    public Valvola() {
        this.aperta = false;
    }
    
    public boolean aperta() {
        return this.aperta;
    }
    
    @Override
    public void run() {
        long time = (long)(Math.random() * 6000);
        try {
            sleep(time);
        } catch (InterruptedException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println("Valvola aperta dopo "+time+"ms!");
        this.aperta = true;
        synchronized(this) {
            notify();
        }
        System.out.println("Svuoto impianto...");
        time = (long)(20000 + 10000*Math.random());
        try {
            sleep(time);
        } catch (InterruptedException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println("Impianto svuotato dopo "+time+"ms!");
        this.aperta = false;
    }
}
