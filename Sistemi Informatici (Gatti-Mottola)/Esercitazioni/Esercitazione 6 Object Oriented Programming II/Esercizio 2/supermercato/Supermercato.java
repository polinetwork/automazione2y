package supermercato;

import java.util.ArrayList;



public class Supermercato {
    private ArrayList<ProdottoInVendita> prodotti;
    private String nome;
    
    public Supermercato(String nome) {
        this.nome = nome;
        this.prodotti = new ArrayList<ProdottoInVendita>();
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void aggiungiProdotto(Prodotto prodotto, int quantita) {
        for(ProdottoInVendita p : this.prodotti) {
            if(prodotto.getId() == p.getProdotto().getId()) {
                return;
            }
        }
        
        ProdottoInVendita prodottoInVendita = new ProdottoInVendita(prodotto, quantita);
        
        this.prodotti.add(prodottoInVendita);
    }
    
    public void stampaProdotti() {
        System.out.println("Benvenuto in " + this.nome);
        System.out.println("");
        System.out.println("Prodotti disponibili: ");
        
        for(ProdottoInVendita prodotto : this.prodotti) {
            System.out.println(prodotto);
        }
    }
    
    public ProdottoInVendita trovaProdotto(int id) {
        for(ProdottoInVendita prodotto: this.prodotti) {
            if(prodotto.getProdotto().getId() == id) {
                return prodotto;
            }
        }
        
        return null;
    }
    
    public void acquistaProdotto(Cliente cliente, int id, int quantita) {
        ProdottoInVendita prodotto = this.trovaProdotto(id);
        if (prodotto != null) {
            ProdottoInVendita acquisto = prodotto.acquista(quantita);
            if (acquisto != null) {
                boolean acquistoABuonFine = cliente.acquista(acquisto);
                if (!acquistoABuonFine) {
                    int quantitaVecchia = prodotto.getQuantita()+quantita;
                    prodotto.setQuantita(quantitaVecchia);
                }
            }
        }
        
    }
}
