package supermercato;

public class ProdottoInVendita {
    
    private Prodotto prodotto;
    private int quantita;
    
    public ProdottoInVendita(Prodotto prodotto, int quantita) {
        this.prodotto = prodotto;
        this.quantita = quantita;
    }
    
    public Prodotto getProdotto() {
        return this.prodotto;
    }
    
    public int getQuantita() {
        return this.quantita;
    }
    
    public void setQuantita(int quantita) {
        if (quantita >= 0) {  // devo controllare che la quantita che vado a mettere sia > 0
            this.quantita = quantita;
        }
    }
    
    public boolean disponibile() {
        return this.quantita > 0;
    }
    
    public ProdottoInVendita acquista(int quantita) {
        if(quantita <= this.quantita) {
            this.quantita -= quantita;
            return new ProdottoInVendita(this.prodotto, quantita);
        }
        
        return null;  // controlla che sia ok
        
    }
    
    @Override
    public String toString() {
        //return this.prodotto + " x"+ this.quantita;
        return this.prodotto.getId() + ": " + this.quantita +" x " + this.prodotto;
    }
}
