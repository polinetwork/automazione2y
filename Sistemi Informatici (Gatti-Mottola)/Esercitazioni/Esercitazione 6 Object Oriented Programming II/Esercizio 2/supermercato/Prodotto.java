package supermercato;

public interface Prodotto {
    
    int getId();
    String getDescrizione();
    boolean vendibile();
    double getPrezzo();
    double getPrezzoScontato();
    double getSconto();
}
