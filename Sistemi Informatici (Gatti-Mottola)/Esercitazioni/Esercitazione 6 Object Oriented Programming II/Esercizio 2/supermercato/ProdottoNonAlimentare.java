package supermercato;

public class ProdottoNonAlimentare implements Prodotto {
    private int id;
    private double prezzo;
    private String descrizione;
    private double sconto;
    
    public ProdottoNonAlimentare(int id, double prezzo, String descrizione) {
        this.id = id;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.sconto = 0;
    }
    
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public double getPrezzo() {
        return this.prezzo;
    }

    @Override
    public String getDescrizione() {
        return this.descrizione;
    }
    
    @Override
    public double getSconto() {
        return this.sconto;
    }

    @Override
    public double getPrezzoScontato() {
        double prezzoScontato = this.prezzo * (1-this.sconto);
        
        return Math.round(prezzoScontato*100.0)/100.0;
    }

    @Override
    public boolean vendibile() {
        return true;
    }
    
    public void setSconto(double sconto) {
        if(sconto < 0 || sconto > 1) {
            return;
        }
        
        this.sconto = sconto;
    }
    
    @Override
    public boolean equals(Object altro) {
        if(this == altro) {
            return true;
        }
        
        if(altro == null) {
            return false;
        }
        
        if(this.getClass() != altro.getClass()) {
            return false;
        }
        
        ProdottoNonAlimentare p = (ProdottoNonAlimentare) altro;
        
        return
                this.id == p.id &&
                this.prezzo == p.prezzo &&
                this.descrizione.equals(p.descrizione) &&
                this.sconto == p.sconto;
    }
    
    @Override
    public String toString() {
        return "ProdottoNonAlimentare(" +
                this.id + ", " + this.descrizione + ", " +
                this.prezzo + ", " + this.getPrezzoScontato() +", " +
                this.vendibile() + ")";
    }
}
