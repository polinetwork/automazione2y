package supermercato;

import java.util.ArrayList;


public class Cliente {
    private String nome;
    private double soldi;
    
    private ArrayList<ProdottoInVendita> acquisti;
    
    public Cliente(String nome, double soldi) {
        this.nome = nome;
        this.soldi = soldi;
        this.acquisti = new ArrayList<ProdottoInVendita>();
    }
    
    public String getNome() {
        return this.nome;
    }
    

    public double getSoldi() {
        return this.soldi;
    }
    
    public void setSoldi(double soldi) {
        this.soldi = soldi;
    }
    
    
    public boolean acquista(ProdottoInVendita prodotto) {
        double costo = prodotto.getProdotto().getPrezzoScontato() * prodotto.getQuantita();
        
        if(costo > this.soldi) {
            return false;
        }
        
        this.soldi -= costo;
        this.acquisti.add(prodotto);
        return true;
    }
    
    public void stampaAcquisti() {
        System.out.println("Prodotti di " + this.nome);
        System.out.println("");
        
        for(ProdottoInVendita prodotto : this.acquisti) {
            System.out.println(prodotto);
        }
    }    
}
