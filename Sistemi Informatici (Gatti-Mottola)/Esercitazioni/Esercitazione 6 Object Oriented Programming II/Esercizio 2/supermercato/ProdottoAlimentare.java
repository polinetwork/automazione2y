package supermercato;

import java.time.LocalDate;

public class ProdottoAlimentare implements Prodotto {
    
    private int id;
    private double prezzo;
    private String descrizione;
    private LocalDate scadenza;
    
    public ProdottoAlimentare(int id, double prezzo, String descrizione, LocalDate scadenza) {
        this.id = id;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.scadenza = scadenza;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public double getSconto() {
        int differenza = LocalDate.now().until(this.scadenza).getDays();
        
        if(differenza <= 1) {
            return 0.7;
        } else if(differenza <= 2) {
            return 0.5;
        } else if(differenza <= 3) {
            return 0.3;
        } else {
            return 0;
        }
    }
    
    @Override
    public double getPrezzo() {
        return this.prezzo;
    }

    @Override
    public double getPrezzoScontato() {
        double sconto = this.getSconto();
        double prezzoScontato = this.prezzo * (1-sconto);
        
        return Math.round(prezzoScontato*100.0)/100.0;
    }

    @Override
    public String getDescrizione() {
        return this.descrizione;
    }
    
    public LocalDate getScadenza() {
        return this.scadenza;
    }
    
    public boolean scaduto() {
        return this.scadenza.isBefore(LocalDate.now());
    }

    @Override
    public boolean vendibile() {
        return !this.scaduto();
    }
    
    @Override
    public boolean equals(Object altro) {
        if(this == altro) {
            return true;
        }
        
        if(altro == null) {
            return false;
        }
        
        if(this.getClass() != altro.getClass()) {
            return false;
        }
        
        ProdottoAlimentare p = (ProdottoAlimentare) altro;
        
        return
                this.id == p.id &&
                this.prezzo == p.prezzo &&
                this.descrizione.equals(p.descrizione) &&
                this.scadenza == p.scadenza;
    }
    
    @Override
    public String toString() {
        return "ProdottoAlimentare(" +
                this.id + ", " + this.descrizione + ", " +
                this.prezzo + ", " + this.getPrezzoScontato() + ", " +
                this.scadenza + ", " + this.vendibile() + ")";
    }
}
