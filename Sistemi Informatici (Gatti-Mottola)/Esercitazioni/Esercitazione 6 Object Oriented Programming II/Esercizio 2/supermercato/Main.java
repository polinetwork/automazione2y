package supermercato;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Supermercato supermercato = new Supermercato("S-Long");
        Cliente cliente = new Cliente("Federico", 200);
        
        // stock magazzino
        ArrayList<Prodotto> prodotti = new ArrayList<Prodotto>();
        prodotti.add(new ProdottoAlimentare(1, 2, "Carota", LocalDate.of(2021, 11, 17)));
        prodotti.add(new ProdottoAlimentare(2, 3, "Patata", LocalDate.of(2021, 11, 16)));
        
        ProdottoNonAlimentare p = new ProdottoNonAlimentare(3, 200, "Televisione");
        p.setSconto(0.10);
        prodotti.add(p);
        prodotti.add(new ProdottoNonAlimentare(4, 3, "Sapone"));
        
        for(Prodotto prodotto: prodotti) {
            supermercato.aggiungiProdotto(prodotto, 100);
        }
        
        supermercato.stampaProdotti();
        
        // Acquisto
        supermercato.acquistaProdotto(cliente, 1, 10);
               
        cliente.stampaAcquisti();
        supermercato.stampaProdotti();
    }
}
