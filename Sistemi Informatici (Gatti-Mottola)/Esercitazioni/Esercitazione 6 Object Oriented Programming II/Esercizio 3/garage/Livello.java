package garage;

import java.util.ArrayList;

public class Livello {
    private int numero_livello;
    private ArrayList<PostoAuto> posti_auto;
    
    public Livello(int num_livello) {
        this.numero_livello = num_livello;
        this.posti_auto = new ArrayList<PostoAuto>();
    }
    
    public void aggiungiPostoAuto(PostoAuto posto_auto) {
        this.posti_auto.add(posto_auto);   
    }
    
    public PostoAuto getPostoAuto(int codice_posto) {
        for (PostoAuto p : this.posti_auto) {
            if (codice_posto == p.getCodicePosto()) {
                return p;
            }
        }
        
        return null;
    }
    
    
    public PostoAuto trovaPostoLibero(String tipo_auto) {

        for (PostoAuto p: this.posti_auto) {
            if (tipo_auto == "lusso" && (p instanceof AutoLusso) && (p.disponibile())) {
                return p;
            }
            else if (tipo_auto == "van" && (p instanceof Van) && p.disponibile()) {
                return p;
            }
            else if (tipo_auto == "normale" && (p instanceof AutoNormale) && p.disponibile()) {
                return p;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        String s = "";
        for (PostoAuto p : this.posti_auto) {
            s += "\t Posto: " + p + "\n";
        }
        return s;
    }
}
