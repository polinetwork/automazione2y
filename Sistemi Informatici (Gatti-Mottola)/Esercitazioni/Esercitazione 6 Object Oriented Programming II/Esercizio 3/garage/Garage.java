package garage;

import java.util.ArrayList;
import java.time.LocalDate;

public class Garage {
    ArrayList<Livello> livelli;
    
    public Garage() {
        this.livelli = new ArrayList<Livello>();
    }
    
    public Livello aggiungiLivello(int piano) {
        Livello livello = new Livello(piano);
        this.livelli.add(livello);
        return livello;
    }
    
    public Abbonamento ingressoAuto(LocalDate data_ingresso, String tipo_abbonamento, String tipo, boolean is_gpl) {
        // 1. Ricerca posto libero
        PostoAuto posto = trovaPostoLibero(tipo, is_gpl);
        if (posto == null) {
            return null;
        }
        
        Abbonamento abbonamento = new Abbonamento(data_ingresso, posto.getCodicePosto(), tipo_abbonamento);
        posto.setDisponibilita(false);
        return abbonamento;
        
    }
    
    public String paga(Abbonamento abbonamento) {
        int codice_posto = abbonamento.getCodicePosto();
        PostoAuto posto = null;
        for (Livello livello: this.livelli) {
            posto = livello.getPostoAuto(codice_posto);
            if (posto != null) {
                break;
            }
        }
        if (posto == null) {
            return "";
        }
        double costo = posto.calcolaCosto(abbonamento);
        posto.setDisponibilita(true);
        return String.valueOf(costo);
    }
    
    public PostoAuto trovaPostoLibero(String tipo, boolean is_gpl) {
        if (is_gpl) {
            PostoAuto posto = this.livelli.get(0).trovaPostoLibero(tipo);
            return posto;
        }
        else {
            PostoAuto posto = null;
            for (Livello livello: this.livelli) {
                posto = livello.trovaPostoLibero(tipo);
                if (posto != null) {
                    return posto;
                }
            }
            return posto;
            
        }
    }
    
    @Override
    public String toString() {
        String s = "";
        for (Livello livello: this.livelli) {
            s += "Livello: \n" + livello + "\n";
        }
        return s;
    }
    
}
