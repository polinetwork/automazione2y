package garage;

import java.time.LocalDate;

public class Abbonamento {
    private LocalDate ingresso;
    private int codice_posto;
    private String tipo_abbonamento;
    
    public Abbonamento(LocalDate ingresso, int codice_posto, String tipo_abbonamento) {
        this.ingresso = ingresso;
        this.codice_posto = codice_posto;
        this.tipo_abbonamento = tipo_abbonamento;
    }
    
    public LocalDate getIngresso () {
        return this.ingresso;
    }
    
    public int getCodicePosto() {
        return this.codice_posto;
    }
    
    public String getTipo() {
        return this.tipo_abbonamento;
    }
}
