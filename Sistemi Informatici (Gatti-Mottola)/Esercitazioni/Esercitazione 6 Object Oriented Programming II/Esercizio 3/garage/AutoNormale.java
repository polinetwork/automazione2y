package garage;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class AutoNormale extends PostoAuto {
    public AutoNormale(int id) {
        super(id);
    }
    
    public double calcolaCosto(Abbonamento abbonamento) {
        String tipo = abbonamento.getTipo();
        LocalDate ingresso = abbonamento.getIngresso();
        double costo;
        if (tipo == "mensile") {
            long numero_mesi = ChronoUnit.MONTHS.between(ingresso, LocalDate.now());
            costo = numero_mesi*100;
        }
        else {
            long numero_giorni= ChronoUnit.DAYS.between(ingresso, LocalDate.now());
            if (numero_giorni <= 5) {
                costo = numero_giorni*5 + 15;
            }
            else {
                costo = numero_giorni*5;
            }
        }
        return costo;
    }
    
    @Override 
    public String toString() {
        return "normale " + this.disponibile();
    }
}
