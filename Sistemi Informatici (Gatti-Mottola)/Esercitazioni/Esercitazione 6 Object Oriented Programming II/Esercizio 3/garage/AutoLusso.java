package garage;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class AutoLusso extends PostoAuto {
    private static final double penale = 30;
    
    public AutoLusso(int id) {
        super(id);
    }
    
    public double calcolaCosto(Abbonamento abbonamento) {
        String tipo = abbonamento.getTipo();
        LocalDate ingresso = abbonamento.getIngresso();
        double costo;
        if (tipo == "mensile") {
            long numero_mesi = ChronoUnit.MONTHS.between(ingresso, LocalDate.now());
            costo = numero_mesi*230;
        }
        else {
            long numero_giorni = ChronoUnit.DAYS.between(ingresso, LocalDate.now());
            if (numero_giorni <= 5) {
                costo = numero_giorni*10 + 40;
            }
            else {
                costo = numero_giorni*10;
            }
        }
        return costo;
    }
    
    @Override 
    public String toString() {
        return "lusso " + this.disponibile();
    }
}
