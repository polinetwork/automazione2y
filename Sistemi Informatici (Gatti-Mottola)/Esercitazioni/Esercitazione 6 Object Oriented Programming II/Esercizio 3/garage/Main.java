package garage;

import java.util.ArrayList;
import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Garage garage = new Garage();

        // Aggiungi livelli
        Livello livello1 = garage.aggiungiLivello(1);
        livello1.aggiungiPostoAuto(new AutoLusso(1));
        livello1.aggiungiPostoAuto(new AutoLusso(2));
        livello1.aggiungiPostoAuto(new AutoLusso(3));
        livello1.aggiungiPostoAuto(new Van(4));
        livello1.aggiungiPostoAuto(new AutoNormale(5));
        livello1.aggiungiPostoAuto(new AutoNormale(6));
        
        Livello livello2 = garage.aggiungiLivello(2);
        livello2.aggiungiPostoAuto(new AutoNormale(7));
        livello2.aggiungiPostoAuto(new AutoNormale(8));
        livello2.aggiungiPostoAuto(new AutoNormale(9));
        livello2.aggiungiPostoAuto(new Van(10));
        livello2.aggiungiPostoAuto(new Van(11));
        
        // Ingresso auto: inserisci data + tipo di abbonamento + tipo auto
        System.out.println("Garage: \n" + garage);
        Abbonamento abbonamento1 = garage.ingressoAuto(LocalDate.of(2021, 9, 19), "mensile", "van", true);
        Abbonamento abbonamento2 = garage.ingressoAuto(LocalDate.of(2021, 11, 18), "giornaliero", "lusso", false);
        Abbonamento abbonamento3 = garage.ingressoAuto(LocalDate.of(2021, 11, 13), "giornaliero", "lusso", false);
        
        
        // Paga: now - ingresso 
        System.out.println("Garage: \n" + garage);
        System.out.println("Pagamento per abbonamento 1: " + garage.paga(abbonamento1));
        System.out.println("Garage: \n" + garage);
        System.out.println("Pagamento per abbonamento 2: " + garage.paga(abbonamento2));
        System.out.println("Garage: \n" + garage);
        System.out.println("Pagamento per abbonamento 3: " + garage.paga(abbonamento3));
        System.out.println("Garage: \n" + garage);
        
        
    }
}
