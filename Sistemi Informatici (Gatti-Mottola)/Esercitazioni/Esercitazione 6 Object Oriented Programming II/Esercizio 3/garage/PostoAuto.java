package garage;

import java.time.LocalDate;


public abstract class PostoAuto {
    private boolean disponibile;
    private int codice_posto;
    
    public PostoAuto(int codice_posto) {
        this.codice_posto = codice_posto;
        this.disponibile = true;
    }
    
    public int getCodicePosto() {
        return this.codice_posto;
    }
    
    public boolean disponibile() {
        return this.disponibile;
    }
    
    public void setDisponibilita(boolean disponibilita) {
        this.disponibile = disponibilita;
    }
    
    public abstract double calcolaCosto(Abbonamento abbonamento);
    
}
